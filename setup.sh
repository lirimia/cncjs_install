#!/bin/bash

echo
echo "**************************************************"
echo "* Updating linux                                 *"
echo "**************************************************"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y


echo
echo "**************************************************"
echo "* Set hostname, timezone and locale               *"
echo "**************************************************"
sudo hostnamectl set-hostname cncjs
sudo sed -i "s|raspberrypi|cncjs|" /etc/hosts > /dev/null 2>&1

sudo timedatectl set-timezone Europe/Bucharest

sudo sed -i "s|# en_GB.UTF-8 UTF-8|en_GB.UTF-8 UTF-8|" /etc/locale.gen
sudo sed -i "s|# en_US.UTF-8 UTF-8|en_US.UTF-8 UTF-8|" /etc/locale.gen
sudo locale-gen

echo '' | tee -a ${HOME}/.bashrc
echo 'export LC_ALL="en_US.UTF-8"' | tee -a ${HOME}/.bashrc

echo '' | sudo tee -a ${HOME}/.bashrc
echo alias ll=\'ls -alh\' | sudo tee -a ${HOME}/.bashrc


echo
echo "**************************************************"
echo "* Install required packages                      *"
echo "**************************************************"
sudo apt-get install -y snmp snmpd
sudo apt-get install -y sshpass
sudo apt-get install -y dos2unix
sudo apt-get install -y pv
sudo apt-get install -y mc
sudo apt-get install -y zip
sudo apt-get install -y unrar-free
sudo apt-get install -y mosquitto-clients
sudo apt-get install -y git
sudo apt-get install -y moreutils
sudo apt-get install -y build-essential
sudo apt-get install -y htop 
sudo apt-get install -y iotop 
sudo apt-get install -y nmon 
sudo apt-get install -y lsof 
sudo apt-get install -y screen

git config --global user.name 'Laur'
git config --global user.email 'lirimia@gmail.com'


echo
echo "**************************************************"
echo "* Scripts                                        *"
echo "**************************************************"
curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/scripts/convertToSVG.py --create-dirs -o ${HOME}/scripts/convertToSVG.py
curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/scripts/gcodeSplit.py --create-dirs -o ${HOME}/scripts/gcodeSplit.py
curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/scripts/system_commands.sh --create-dirs -o ${HOME}/scripts/system_commands.sh
curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/scripts/build.sh --create-dirs -o ${HOME}/scripts/build.sh

chmod +x ${HOME}/scripts/*

echo '@reboot /home/biqu/scripts/system_commands.sh &' | sudo tee -a /var/spool/cron/biqu
/usr/bin/crontab /var/spool/cron/biqu


echo
echo "**************************************************"
echo "* Docker and docker-compose                      *"
echo "**************************************************"
export LC_ALL="en_GB.UTF-8"

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $(whoami)
sudo usermod -aG bluetooth $(whoami)
sudo chmod 666 /var/run/docker.sock
rm get-docker.sh

sudo apt install -y python3-pip python3-dev docker-compose
sudo pip3 install -U ruamel.yaml==0.16.12 blessed


echo
echo "**************************************************"
echo "* Start containers                               *"
echo "**************************************************"
mkdir $HOME/gcode

curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/cncjs/cncrc --create-dirs -o ${HOME}/.cncrc
curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/docker-compose.yml --create-dirs -o ${HOME}/docker-compose.yml

docker-compose up -d


echo
echo "**************************************************"
echo "* Cleaning linux                                 *"
echo "**************************************************"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get autoremove -y
sudo apt-get autoclean -y


echo
echo "**************************************************"
echo "* Append script VersionID to log file            *"
echo "**************************************************"
git clone --quiet https://bitbucket.org/lirimia/cncjs_install.git

cd cncjs_install/
version=$(git log --oneline | head -1 | awk '{print $1}')
cd ..

rm -rf cncjs_install

echo "Logfile for CNCjs Install script version $version"
echo "Installation completed!"

sudo reboot
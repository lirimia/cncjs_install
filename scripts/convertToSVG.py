#!/usr/bin/python

import os
import shutil

rootFolder = '/home/pi/gcode'

cncToSvg = '/home/pi/cnc-to-svg/'
gerberToSvg = '/home/pi/gerber-to-svg/bin/gerber2svg  -q -o'

if __name__ == '__main__':
    for(root, folders, files) in os.walk(rootFolder):
        for file in files:
            if '.iso' in file and 'soldermask' in file:
                print('Creating SVG from %s' % file)

                with open(os.path.join(root, file), 'r') as input:
                    content = input.readlines()

                output = open(os.path.join(cncToSvg, 'tests', 'CAMOutput.nc'), 'w+')
                for line in content:
                    line = line.strip('\n')

                    if line == '%':
                        continue

                    output.write(line + '\n')
                output.close()

                currentDir = os.getcwd()
                os.chdir(os.path.join(cncToSvg, 'tests'))
                os.system('node tests.js >> /dev/null')
                os.chdir(currentDir)

                shutil.copyfile(os.path.join(cncToSvg, 'tests', 'CAMOutput.svg'), os.path.join(rootFolder, file.split('.')[0] + '.svg'))

            if '.gbr' in file:
                # if 'silkscreen' in file or 'soldermask' in file or 'solderpaste' in file:
                if 'soldermask' in file or 'silkscreen' in file or 'solderpaste' in file:
                    print('Creating SVG from %s' % file)

                    if not '\ ' in root:
                        root = root.replace(' ', '\ ')

                    cmd = gerberToSvg + ' ' + root + ' ' + os.path.join(root, file)
                    os.system(cmd)

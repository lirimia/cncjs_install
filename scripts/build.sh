#!/bin/bash

function prepare() {
    # prepare repository
    if [ -d $HOME/cncjs/ ]; then
        rm -rf $HOME/cncjs
    fi

    git clone https://github.com/cncjs/cncjs.git
    cd cncjs

    # get desired version
    VERSION=$(git log --no-walk --tags --pretty="%h %d %s" --decorate=full | awk -F '\/' '{print $3}' | awk -F ')' '{print $1}' | head -1)
    echo $VERSION | cut -d 'v' -f 2 > VERSION

    TIP=$(git log --no-walk --tags --pretty="%h %d %s" --decorate=full | grep $VERSION | cut -f 1 -d ' ')
    git checkout $TIP

    cp Dockerfile Dockerfile.bk

    # get Makefile from BitBucket
    curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/cncjs/Makefile -o $HOME/cncjs/Makefile
}

function compile() {
    ADDR=$1
    USER=$2

    # cleanup
    if [ -d $HOME/.nvm/ ]; then
        rm -rf $HOME/.nvm
    fi

    if [ -d $HOME/.npm/ ]; then
        rm -rf $HOME/.npm
    fi

    if [ -d $HOME/.yarn/ ]; then
        rm -rf $HOME/.yarn
    fi

    # install nvm
    git clone https://github.com/creationix/nvm.git $HOME/.nvm
    . $HOME/.nvm/nvm.sh

    # install Node.js
    NODE_VERSION=$(nvm ls-remote | tail -1 | awk '{print $1}')
    echo $NODE_VERSION > NODE
    scp NODE $USER@$ADDR:cncjs/
    rm NODE
    nvm install $NODE_VERSION

    # install NPM
    npm install -g npm@latest

    # install CNCjs
    scp $USER@$ADDR:cncjs/VERSION .
    VERSION=$(cat VERSION)
    rm VERSION

    npm install -g cncjs@$VERSION

    # transfer binaries
    SOURCE=$(which node | awk -F 'bin/' '{print $1}')
    cd $SOURCE/lib/node_modules/cncjs
    rsync -av --delete --include='dist/***' --include='node_modules/***' --include='bin/***' --exclude='*' ./ $USER@$ADDR:/home/$USER/cncjs/
}

function build() {
    # update Dockerfile
    cp Dockerfile.bk Dockerfile

    NODE_OLD=$(cat Dockerfile | grep 'ENV NODE_VERSION' | head -1 | awk -F 'ENV NODE_VERSION ' '{print $2}')
    NODE_NEW=$(cat NODE)
    sed -i "s|$NODE_OLD|$NODE_NEW|" Dockerfile

    # build new image
    make build

    # re-create container with the new image
    docker stop cncjs
    docker rm cncjs
    docker run -ti \
    --name cncjs \
    --detach \
    --privileged \
    --restart unless-stopped \
    --publish 80:8000 \
    -v ${HOME}/gcode:/watchdir \
    -v ${HOME}/.cncrc:/root/.cncrc \
    -v /dev:/dev \
    lirimia/cncjs:latest

    echo
    echo "Upon testing the new image, it should be pushed to Dockerhub using the following command:"
    echo "make push"
    echo
}

if [ $# -eq 0 ]; then
    IPADDR=$(ifconfig | grep 10.0 | grep netmask | head -1 | awk '{print $2}')

    prepare

    echo
    echo "Run the following command on the remote machine (Mac):"
    echo "curl -L https://bitbucket.org/lirimia/cncjs_install/raw/master/scripts/build.sh | bash -s -- $IPADDR $(whoami)"

    echo
    read -p "Upon completion, return to this shell and press Enter to continue"

    build
elif [ $# -eq 2 ]; then
    compile $1 $2
elif [ $# -eq 1 ] && [ $1 = 'build' ]; then
    build
fi

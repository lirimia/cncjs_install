#!/bin/bash

while true; do
    if [ -f $HOME/gcode/reboot ]; then
        sudo rm $HOME/gcode/reboot
        sleep 5
        sudo reboot
    fi

    if [ -f $HOME/gcode/poweroff ]; then
        sudo rm $HOME/gcode/poweroff
        sleep 5
        sudo poweroff
    fi

    sleep 1
done
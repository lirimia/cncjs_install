
import os

rootFolder = '/home/pi/gcode'
rootFolder = '/Users/lairimia/Desktop'

for (root, folders, files) in os.walk(rootFolder):
    for file in files:
        if '.iso' in file:
            isHeader = True
            header = list()

            fileIndex = 0

            # parse input file
            with open(os.path.join(rootFolder, file), 'r') as ih:
                lines = ih.readlines()

            for line in lines:
                line = line.strip()

                if 'M06' in line:
                    continue

                if '( Tool ' not in line and isHeader:
                    header.append(line)
                elif '( Tool ' in line:
                    if isHeader: 
                        isHeader = False
                    else:
                        oh.write("%s%s%s" % ("M02", "\n", "%"))
                        oh.close()
                    
                    fileIndex += 1
                    tool = line.split('"')[1]
                    print("Writing data to %s_%d_%s.iso" % (file.split('.')[0], fileIndex, tool))

                    oh = open(os.path.join(rootFolder, file.split('.')[0] + "_" + str(fileIndex) + "_" + tool + ".iso"), 'w')
                    for headerLine in header:
                        oh.write("%s%s" % (headerLine, "\n"))
                    oh.write("%s%s" % (line, "\n"))
                else:
                    oh.write("%s%s" % (line, "\n"))

            oh.close()
